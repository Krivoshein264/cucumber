package step_definitions;

import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static org.junit.Assert.assertEquals;

public class ProductPageStepsDef {

    private static final By PRODUCT_NAME = By.xpath("//form[@name='product']//h1");
    private static final By PRISE = By.xpath("//span[@id='block_price']");
    private static final By WEIGHT = By.xpath("//span[@id='block_weight']");
    static String nameProduct = $(PRODUCT_NAME).text();
    static String priceProduct = $(PRISE).getText();
    static String weightProduct = $(WEIGHT).text();

    @И("^сравнить в нем данные из переменных$")
    public void attributesComparison() {
        if (nameProduct.startsWith(ShoppingPageStepDef.name)) {
            assertEquals(priceProduct, ShoppingPageStepDef.price);
            assertEquals(weightProduct, ShoppingPageStepDef.weight);
        } else {
            System.out.println("Название не прошло");
        }
    }
}
