package step_definitions;

import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class MainPageStepsDef {
    private static final By SHOPPING_BUTTON = By.xpath("//div/preceding-sibling::a[@href='/index.php/magazin']");
    private static final By BLOG_BUTTON = By.xpath("//ul[@class='uk-navbar-nav']//a[contains(text(), 'Блог Джесси')]");
    @И("^открыть страницу$")
    public void goToShop() {
        open("https://qahacking.guru/");
        $(SHOPPING_BUTTON).click();
    }
    @И("^перейти в магазин$")
    public void goToHome() {
        $(SHOPPING_BUTTON).click();
    }
    @И("^перейти в блог$")
    public void goToBlog() { $(BLOG_BUTTON).click(); }
}
