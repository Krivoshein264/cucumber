package step_definitions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class ShoppingPageStepDef {

    private static final By PRODUCT = By.xpath(".//div[@class='name']//a");
    private static final By PRODUCT_NAME = By.xpath("//div[contains(@class,'productitem')]/div[@class='name']/a");
    private static final By PRISE = By.xpath(".//div[@class='jshop_price']//span");
    private static final By WEIGHT = By.xpath(".//span[contains(text(),'Kg')]");
    SelenideElement product;
    static String name;
    static String price;
    static String weight;
    @И("^записать в переменные параметры Название, Цена, Вес товара \"(.*)\"$")
    public void extractAttributes(String productName) {
        product = $$(PRODUCT_NAME).findBy(Condition.text(productName)).parent().parent();
        name = productName;
        price = product.$(PRISE).getText();
        weight = product.$(WEIGHT).text();
    }
    @И("^выбрать продукт$")
    public void selectProduct() {
        product.$(PRODUCT).click();
    }
}
