package step_definitions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class BlogPageStepsDef {
    static String name;
private static final By TEXT_NAME = By.xpath("//h3[@class='el-title uk-h4 uk-margin-top uk-margin-remove-bottom']//a");
private static final By GET_TEXT_NAME = By.xpath(".//h3[@class='el-title uk-h4 uk-margin-top uk-margin-remove-bottom']//a");
@И("^записать в переменную название и открыть \"(.*)\"$")
public void extractName(String textName) {
    SelenideElement elements = $$(TEXT_NAME).findBy(Condition.text(textName)).parent().parent().parent();
    name = elements.$(GET_TEXT_NAME).getText();
    elements.$(GET_TEXT_NAME).click();
}
}
