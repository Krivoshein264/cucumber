package step_definitions;

import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static org.junit.Assert.assertEquals;

public class TextPageStepsDef {
    private static final By BLOG_NAME = By.xpath("//h1[@class='uk-margin-small uk-text-center uk-scrollspy-inview uk-animation-fade']");
    @И("^стравнить название в блоге с названием из переменной$")
    public void checkName() {
        assertEquals(BlogPageStepsDef.name, $(BLOG_NAME).text());
    }
}
